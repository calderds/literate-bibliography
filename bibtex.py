#!/usr/bin/env python3
import bibtexparser as btx
import os
import datetime

with open("library.bib") as bibtex_file:
    db = btx.load(bibtex_file)

db_dict = db.entries_dict

org_str = ""

with open("title.org") as org_file:
    org_str += org_file.read()

org_str += f"#+DATE: {datetime.datetime.now():%B %d, %Y}\n\n"

with open("introduction.org") as org_file:
    org_str += org_file.read()

new_bib = ""

for k in db_dict.keys():
    new_db = btx.bibdatabase.BibDatabase()

    try:
        _ = db_dict[k].pop('note')
    except:
        pass

    try:
        _ = db_dict[k].pop('url')
    except:
        pass

    try:
        _ = db_dict[k].pop('file')
    except:
        pass

    new_db.entries = [db_dict[k]]

    title = db_dict[k]['title']
    try:
        author = db_dict[k]['author']
        title_str = f"*** {author.split('and')[0]}et al: {title.replace('{', '').replace('}', '')}\n\n"
    except:
        title_str = f"*** {title.replace('{', '').replace('}', '')}\n\n"

    org_str += title_str

    org_str += "#+begin_src LaTeX\n"

    org_str += btx.dumps(new_db)

    new_bib += btx.dumps(new_db)

    org_str += "#+end_src\n\n"

with open("README.org", "w") as dest_file:
    dest_file.write(org_str)

with open("new_library.bib", "w") as new_bib_file:
    new_bib_file.write(new_bib)

try:
    os.system("mv new_library.bib library.bib")
except:
    pass
